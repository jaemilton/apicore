﻿using coreApi.Common.Interfaces;
using coreApi.Util;
using coreApi.Util.Enums;
using coreApi.Util.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace coreApi.Common
{
    public class ScopedService 
    {
        private readonly IServiceProvider _services;
        public ScopedService(IServiceProvider services)
        {
            _services = services;
        }
        public async Task ScopedExecutionAsync(BackgroundTaskBase workItem)
        {
            BackgroundTaskLog customLog = new BackgroundTaskLog();
            customLog.StartDateTime = DateTime.Now;
            customLog.Assembly = workItem.IDoWorkAsyncType.FullName;
            customLog.OriginRequestInfo = workItem.OriginalRequestInfo;
            using (var scope = _services.CreateScope())
            {
                var logger = scope.ServiceProvider.GetService<ICustomLogger>();
                try
                {
                    var iDoWorkTask = (IDoWorkAsync)scope.ServiceProvider.GetService(workItem.IDoWorkAsyncType);

                    await iDoWorkTask.DoWorkAsync(workItem.StoppingToken, workItem.Parameters);
                }
                catch (Exception ex)
                {
                    logger.WriteLog(ex, $"Error occurred executing {nameof(workItem.IDoWorkAsyncType.Name)}.");
                }
                finally
                {
                    customLog.FinishedDateTime = DateTime.Now;
                    //Save logs here
                    await LogHelper.WriteLogsAsync(LoggerType.Console, null, customLog, logger);
                }
            }

        }
    }
}
