﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace coreApi.Common
{
    /// <summary>
    /// To support IDisposable, pass true to constructor and call:
    /// 
    ///         AutoDispose(IDisposable) for each disposable at time of creation,
    ///         
    /// Or override these as needed:
    /// 
    ///         FreeManagedResources() and 
    ///         FreeUnmanagedResources()
    ///         
    /// Multi-thread safe.
    /// </summary>
    public abstract class SimplifiedDisposableBase : IDisposable
    {
        /// <summary>
        /// Flag for IDisposable
        /// </summary>
        protected bool _isDisposed = false;

        /// <summary>
        /// List of items that should be Dispose() when the instance is Disposed()
        /// </summary>
        private List<IDisposable> _autoDisposables = new List<IDisposable>();

        /// <summary>
        /// Constructor
        /// </summary>
        public SimplifiedDisposableBase()
        {
        }

        /// <summary>
        /// Finalizer (needed for freeing unmanaged resources and adds a check a Dispose() check for managed resources).
        /// </summary>
        ~SimplifiedDisposableBase()
        {
            // Warning:  An exception here will end the application.
            // Do not attempt to lock to a possibly finalized object within finalizer
            // http://stackoverflow.com/questions/4163603/why-garbage-collector-takes-objects-in-the-wrong-order            

            string errMessages = string.Empty;
            try
            {
                errMessages = String.Format("Warning:  Finalizer was called on class '{0}' (base class '{1}').  " +
                    "IDisposable's should usually call Dispose() to avoid this.  (IsDisposed = {2})",
                    GetType().FullName,
                    typeof(SimplifiedDisposableBase).FullName,
                    _isDisposed);

                Debug.WriteLine(errMessages);

                Dispose(false);     // free any unmanaged resources                
            }
#if DEBUG
            catch (Exception ex)
            {
                errMessages = "Fatal:  Exception occurred within Finalizer ~" + GetType().FullName + "()." + errMessages;
                Debug.WriteLine(errMessages + "  " + ex.Message);

                // Verified that this exception appears in Windows Event Log and includes the originating class message and StackTrace[0]
                throw new Exception(errMessages, ex);
            }
#else
            catch (Exception) 
            {
                /* Don't exit the application */ 
            }
#endif
        }

        /// <summary>
        /// Add an managed item to be automatically disposed when the class is disposed.
        /// </summary>
        /// <param name="disposable"></param>
        /// <returns>The argument</returns>
        public T AutoDispose<T>(T disposable) where T : IDisposable
        {
            lock (_autoDisposables)
                _autoDisposables.Add(disposable);

            return disposable;
        }

        /// <summary>
        /// Derived class can override and chain for support of IDisposable managed resources.
        /// </summary>
        protected virtual void FreeManagedResources()
        {
            lock (_autoDisposables)
            {
                _autoDisposables
                    .ForEach(d => d.Dispose());

                _autoDisposables.Clear();
            }
        }

        /// <summary>
        /// Derived class can optionally override for support of IDisposable unmanaged resources.
        /// </summary>
        protected virtual void FreeUnmanagedResources() { }

        /// <summary>
        /// Standard IDisposable Implmentation
        /// </summary>
        public void Dispose()
        {
            Dispose(true);  // calling multiple times is okay
            GC.SuppressFinalize(this);  // http://stackoverflow.com/questions/12436555/calling-suppressfinalize-multiple-times is okay
        }

        /// <summary>
        /// Dispose
        /// </summary>
        protected virtual void Dispose(bool isDisposing)
        {
            if (!isDisposing) // if called from finalizer, do not use lock (causes exception)
            {
                if (!_isDisposed)
                {
                    FreeUnmanagedResources();   // always free these                
                    _isDisposed = true;
                }
                return;
            }

            // Remainder is called from IDisposable (not finalizer)

            // Based on "Implemenent IDisposable Correctly" 
            // http://msdn.microsoft.com/en-us/library/ms244737.aspx

            lock (_autoDisposables)
            {
                if (_isDisposed)
                    return; // the docs specifically state that Dispose() must be callable multiple times without raising an exception

                try
                {
                    try
                    {
                        FreeManagedResources();
                    }
                    finally
                    {
                        FreeUnmanagedResources();   // always free these
                    }
                }
                finally
                {
                    _isDisposed = true;
                }
            }
        }
    }
}
