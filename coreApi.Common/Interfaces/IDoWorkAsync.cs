﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace coreApi.Common.Interfaces
{
    public interface IDoWorkAsync
    {
        public Task DoWorkAsync(CancellationToken stoppingToken, params object[] parameters);
    }
}
