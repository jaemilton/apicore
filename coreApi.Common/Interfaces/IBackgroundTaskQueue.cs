﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using coreApi.Common.BackgroundTasks;

namespace coreApi.Common.Interfaces
{
    // <summary>
    /// Samble from https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-5.0&tabs=visual-studio
    /// </summary>
    public interface IBackgroundTaskQueue
    {
        ValueTask QueueBackgroundWorkItemAsync(BackgroundTaskBase task);
        ValueTask<BackgroundTaskBase> DequeueAsync(CancellationToken cancellationToken);
    }
}
