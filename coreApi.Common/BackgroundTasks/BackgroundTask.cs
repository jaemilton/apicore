﻿using coreApi.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace coreApi.Common.BackgroundTasks
{
    public class BackgroundTask<T> : BackgroundTaskBase where T : IDoWorkAsync
    {
        public override Type IDoWorkAsyncType => typeof(T);
    }
}
