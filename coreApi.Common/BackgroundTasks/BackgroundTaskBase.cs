﻿using coreApi.Util;
using coreApi.Util.RequestsInfo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace coreApi.Common.Interfaces
{
    public abstract class BackgroundTaskBase
    {
        public CancellationToken StoppingToken { get; set; }
        public object[] Parameters { get; set; }

        public abstract Type IDoWorkAsyncType { get; }

        public RequestInfo OriginalRequestInfo { get; set; }
    }
}
