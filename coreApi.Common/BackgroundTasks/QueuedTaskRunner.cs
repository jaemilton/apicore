﻿using coreApi.Common.BackgroundTasks;
using coreApi.Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace coreApi.Common.BackgroundTasks
{
    public class QueuedTaskRunner : BackgroundService
    {
        private readonly IBackgroundTaskQueue _taskQueue;
        private readonly ScopedService _scopedService;

        public QueuedTaskRunner
        (
            IBackgroundTaskQueue taskQueue,
            ScopedService scopedService
        )
        {
            _taskQueue = taskQueue;
            _scopedService = scopedService;
        }

        public async ValueTask AddTaskAsync<T>(CancellationToken stoppingToken, params object[] parameters) where T : IDoWorkAsync
        {
            // Enqueue a background work item
            await _taskQueue.QueueBackgroundWorkItemAsync
            (
                new BackgroundTask<T>()
                {
                    Parameters = parameters,
                    StoppingToken = stoppingToken
                }
            );
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var workItem =
                    await _taskQueue.DequeueAsync(stoppingToken);

                    await _scopedService.ScopedExecutionAsync(workItem);
                }
                catch (OperationCanceledException)
                {
                    // Prevent throwing if the Delay is cancelled
                }
                
            }
        }

        
    }
}
