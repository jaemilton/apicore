﻿using coreApi.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace coreApi.Common.BackgroundTasks
{
    public class BackgroundTaskQueue : IBackgroundTaskQueue
    {
        private readonly Channel<BackgroundTaskBase> _queue;

        public BackgroundTaskQueue(int capacity)
        {
            // Capacity should be set based on the expected application load and
            // number of concurrent threads accessing the queue.            
            // BoundedChannelFullMode.Wait will cause calls to WriteAsync() to return a task,
            // which completes only when space became available. This leads to backpressure,
            // in case too many publishers/calls start accumulating.
            var options = new BoundedChannelOptions(capacity)
            {
                FullMode = BoundedChannelFullMode.Wait
            };
            _queue = Channel.CreateBounded<BackgroundTaskBase>(options);
        }

        public async ValueTask QueueBackgroundWorkItemAsync(BackgroundTaskBase workItem)
        {
            await _queue.Writer.WriteAsync(workItem);
        }

        public async ValueTask<BackgroundTaskBase> DequeueAsync(CancellationToken cancellationToken) 
        {
            var workItem = await _queue.Reader.ReadAsync(cancellationToken);

            return workItem;
        }

       
    }



}
