﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;

namespace coreApi.Workflow
{
    public abstract class Step
    {
        private Step _parent;
        private Dictionary<string, object> _parameters;
        protected Step() { }

        public void InitializeStep
        (
            Dictionary<string, object> parameters,
            Step parent
        )
        {
            _parent = parent;
            _parameters = parameters;
        }


        public static Step Create<T>
        (
            IServiceProvider serviceProvider,  
            Dictionary<string, object> parameters, 
            Step parent = null
        ) where T: Step
        {
            var newStep = serviceProvider.GetService<T>();
            newStep.InitializeStep(parameters, parent);
            return newStep;
        }
    }
}
