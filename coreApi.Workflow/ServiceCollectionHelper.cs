﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace coreApi.Workflow
{
    public static class ServiceCollectionHelper
    {
        private static IServiceProvider _serviceProvider;
        public static void StartServiceCollectionHelper(this IServiceCollection services)
        {
            _serviceProvider = services.BuildServiceProvider();
        }

        public static T GetService<T>()
        {
            return _serviceProvider.GetService<T>();
        }
    }
}
