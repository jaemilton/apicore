﻿using coreApi.Models.Enitities.RelatedData;
using coreApi.Repository.Context;
using coreApi.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace coreApi.Repository
{
    public class RelatedDataRepository : IRelatedDataRepository
    {
        protected RelatedDataDBContext _dbContext;

        public RelatedDataRepository(RelatedDataDBContext context)
        {
            _dbContext = context;
        }

        public void SaveSmsMessage(MessageSmsEntity smsMessage)
        {
            _dbContext.MessagesSms.Add(smsMessage);
        }

        public void SaveEmailMessage(MessageEmailEntity emailMessage)
        {
            _dbContext.MessagesEmail.Add(emailMessage);
        }


        public void SaveKeySmsMessage(MessageKeySmsEntity messageKeySms)
        {
            _dbContext.MessageKeySms.Add(messageKeySms);
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

    }
}
