﻿using coreApi.Models.Enitities.RelatedData;
using System.Threading.Tasks;

namespace coreApi.Repository.Interfaces
{
    public interface IRelatedDataRepository
    {
        void Commit();
        void SaveEmailMessage(MessageEmailEntity emailMessage);
        void SaveSmsMessage(MessageSmsEntity smsMessage);

        void SaveKeySmsMessage(MessageKeySmsEntity messageKeySms);
    }
}