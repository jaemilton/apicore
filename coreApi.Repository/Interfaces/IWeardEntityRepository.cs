﻿using coreApi.Models.Enitities.Weard;

namespace coreApi.Repository.Interfaces
{
    public interface IWeardEntityRepository
    {
        void AddPrincipal(PrincipalWeardEntity entity);
        void AddSecundary(SecundaryWeardEntity entity);
        void Save();
    }
}