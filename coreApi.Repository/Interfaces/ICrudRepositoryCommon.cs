
using coreApi.Models.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace coreApi.Repository.Interfaces
{
    public interface ICrudRepositoryCommon<TEntity> 
        where TEntity : class
    {
        Task<List<TEntity>> SelectAllAsync();
        Task CreateAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
    }
}