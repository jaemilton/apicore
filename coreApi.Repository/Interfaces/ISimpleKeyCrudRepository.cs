
using coreApi.Models.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace coreApi.Repository.Interfaces
{
    public interface ISimpleKeyCrudRepository<TEntity, TKey> :ICrudRepositoryCommon<TEntity>
        where TEntity : class,  IEntityKey<TKey>
    {
        Task<TEntity> SelectByIdAsync(TKey id);
    }
}