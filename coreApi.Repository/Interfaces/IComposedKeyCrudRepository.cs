
using coreApi.Models.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace coreApi.Repository.Interfaces
{
    public interface IComposedKeyCrudRepository<TEntity> :ICrudRepositoryCommon<TEntity>
        where TEntity : class,  IEntityComposedKey<TEntity>
    {
        Task<TEntity> SelectByIdAsync(params object[] keyValues);

    }
}