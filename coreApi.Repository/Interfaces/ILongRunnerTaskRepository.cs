﻿using coreApi.Models.Enitities;

namespace coreApi.Repository.Interfaces
{
    public interface ILongRunnerTaskRepository
    {
        void SaveSmsMessage(TaskRunner task);
        public void Commit();
    }
}