﻿using coreApi.Models.Enitities.Weard;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace coreApi.Repository.Context
{
    public class WeardDbContext : DbContext
    {
        private readonly DbContextOptions _options;
        private readonly IConfiguration _configuration;
        public WeardDbContext(IConfiguration configuration, DbContextOptions<WeardDbContext> options) : base(options)
        {
            _options = options;
            _configuration = configuration;
        }

        public DbSet<PrincipalWeardEntity> Principal { get; set; }
        public DbSet<SecundaryWeardEntity> Secundary { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("default"));
            //https://docs.microsoft.com/en-us/ef/core/performance/performance-diagnosis?tabs=simple-logging%2Cload-entities
            //When the logging level is set at LogLevel.Information, EF emits a log message for each command execution with the time taken:
            //optionsBuilder.LogTo(Console.WriteLine, LogLevel.Debug);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<SecundaryWeardEntity>().HasKey(x => new { x.Id, x.CreatedDate });

            base.OnModelCreating(modelBuilder);

        }
    }
}
