﻿using coreApi.Models.Enitities.RelatedData;
using coreApi.Models.ValueGenerators;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace coreApi.Repository.Context
{
    public class RelatedDataDBContext : DbContext
    {
        private readonly DbContextOptions _options;
        private readonly IConfiguration _configuration;
        public RelatedDataDBContext(IConfiguration configuration, DbContextOptions<RelatedDataDBContext> options) : base(options)
        {

            _options = options;
            _configuration = configuration;
        }
        
        public DbSet<MessageEntity> Messages { get; set; }
        public DbSet<MessageSmsEntity> MessagesSms { get; set; }
        public DbSet<MessageEmailEntity> MessagesEmail { get; set; }

        public DbSet<MessageKeySmsEntity> MessageKeySms { get; set; }

        public DbSet<SituationTypeEntity> Situation { get; set; }

        public DbSet<PrincipalEntity> Principal { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("default"));
            //https://docs.microsoft.com/en-us/ef/core/performance/performance-diagnosis?tabs=simple-logging%2Cload-entities
            //When the logging level is set at LogLevel.Information, EF emits a log message for each command execution with the time taken:
            //optionsBuilder.LogTo(Console.WriteLine, LogLevel.Debug);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<PrincipalEntity>().HasKey(x => new { x.CodeP1, x.CodeP2 });
            modelBuilder.Entity<PrincipalEntity>()
                .HasOne(x => x.Situation)
                .WithOne()
                .HasForeignKey<PrincipalEntity>(x => x.SituationId);

            modelBuilder.Entity<MessageEntity>()
                .HasOne(x => x.Principal)
                .WithOne()
                .HasForeignKey<MessageEntity>(x => new { x.CodeP1, x.CodeP2 });

            modelBuilder.Entity<MessageEntity>()
                .Property(x => x.ExternalMessageCode)
                .HasValueGenerator<ExternalMessageCodeGenerator>();



            modelBuilder.Entity<MessageEntity>()
                .Property(x => x.InsertDate)
                .HasDefaultValueSql("getdate()");

            modelBuilder.Entity<MessageSmsEntity>()
                .HasOne(x => x.Message)
                .WithOne()
                .HasForeignKey<MessageSmsEntity>(x => x.Id);

            modelBuilder.Entity<MessageEmailEntity>()
               .HasOne(x => x.Message)
               .WithOne()
               .HasForeignKey<MessageEmailEntity>(x => x.Id);



            modelBuilder.Entity<MessageKeySmsEntity>()
             .HasKey(x => new { x.CodeP1, x.CodeP2, x.KeyDate, x.Key });

            modelBuilder.Entity<MessageKeySmsEntity>()
               .Property(x => x.KeyDate)
               .ValueGeneratedOnAdd();

            modelBuilder.Entity<MessageKeySmsEntity>()
               .HasOne(x => x.SmsMessage)
               .WithOne()
               .HasForeignKey<MessageKeySmsEntity>(x => x.IdSmsMessage);


            modelBuilder.Entity<MessageKeySmsEntity>()
                .Property(x => x.Key)
                .HasValueGenerator<SmsKeyGenerator>();

            modelBuilder.Entity<MessageKeySmsEntity>()
                .Property(x => x.KeyDate)
                .ValueGeneratedOnAdd();
            //.HasDefaultValueSql("getdate()");

            base.OnModelCreating(modelBuilder);

        }

  

        public override int SaveChanges()
        {
            var messageKeySmsEntityEntries = ChangeTracker
                .Entries()
                .Where(e => 
                        (
                            e.Entity.GetType() == typeof(MessageEntity) ||
                            e.Entity.GetType() == typeof(MessageKeySmsEntity)
                        ) &&
                        (
                            e.State == EntityState.Added
                            || e.State == EntityState.Modified
                        ));

            foreach (var entityEntry in messageKeySmsEntityEntries)
            {
                if (entityEntry.Entity.GetType() == typeof(MessageEntity))
                    ((MessageEntity)entityEntry.Entity).InsertDate = DateTime.Now;
                else
                    ((MessageKeySmsEntity)entityEntry.Entity).KeyDate = DateTime.Now;
            }

            return base.SaveChanges();
        }
    }
}

