﻿using coreApi.Models;
using coreApi.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace coreApi.Repository.Context
{
    public class ConcurrencyDBContext: DbContext
    {
        public ConcurrencyDBContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<ConcurrencyEntity> Concurrencies { get; set; }

       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var x = Activator.CreateInstance<ConcurrencyEntity>();
            modelBuilder.Entity<ConcurrencyEntity>()
                .HasKey(ConcurrencyEntity.KeyExpression);
        }
    }
}
