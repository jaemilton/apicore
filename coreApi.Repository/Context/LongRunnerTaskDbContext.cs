﻿using coreApi.Models.Enitities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace coreApi.Repository.Context
{
    public class LongRunnerTaskDbContext : DbContext
    {

        public LongRunnerTaskDbContext(DbContextOptions options) : base(options)
        {
        }
        
        //public LongRunnerTaskDbContext(IConfiguration configuration, DbContextOptions<LongRunnerTaskDbContext> options) : base(options)
        //{

        //    _options = options;
        //    _configuration = configuration;
        //}


        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(_configuration.GetConnectionString("default"));
        //    //https://docs.microsoft.com/en-us/ef/core/performance/performance-diagnosis?tabs=simple-logging%2Cload-entities
        //    //When the logging level is set at LogLevel.Information, EF emits a log message for each command execution with the time taken:
        //    //optionsBuilder.LogTo(Console.WriteLine, LogLevel.Debug);
        //    base.OnConfiguring(optionsBuilder);
        //}

        public DbSet<TaskRunner> Tasks { get; set; }


    }
}
