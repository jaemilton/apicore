using coreApi.Models;
using coreApi.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace coreApi.Repository.Context
{
    public class AppDBContext: DbContext
    {
        private readonly DbContextOptions _options;
        private readonly IConfiguration _configuration;
        public AppDBContext(IConfiguration configuration, DbContextOptions<AppDBContext> options) : base(options)
        {

            _options = options;
            _configuration = configuration;
        }
        public DbSet<MemberEntity> Members { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("default"));
            //https://docs.microsoft.com/en-us/ef/core/performance/performance-diagnosis?tabs=simple-logging%2Cload-entities
            //When the logging level is set at LogLevel.Information, EF emits a log message for each command execution with the time taken:
            //optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            base.OnModelCreating(modelBuilder);
        }

    }
}