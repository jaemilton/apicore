﻿using coreApi.Models.Enitities;
using coreApi.Repository.Context;
using coreApi.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace coreApi.Repository
{
    public class LongRunnerTaskRepository : ILongRunnerTaskRepository
    {
        protected LongRunnerTaskDbContext _dbContext;

        public LongRunnerTaskRepository(LongRunnerTaskDbContext context)
        {
            _dbContext = context;
        }

        public void SaveSmsMessage(TaskRunner task)
        {
            _dbContext.Tasks.Add(task);
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }
    }
}
