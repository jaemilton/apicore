﻿using coreApi.Models.Enitities.Weard;
using coreApi.Repository.Context;
using coreApi.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace coreApi.Repository
{
    public class WeardEntityRepository : IWeardEntityRepository
    {
        protected WeardDbContext _dbContext;

        public WeardEntityRepository(WeardDbContext context)
        {
            _dbContext = context;
        }

        public void AddPrincipal(PrincipalWeardEntity entity)
        {
            _dbContext.Principal.Add(entity);
        }

        public void AddSecundary(SecundaryWeardEntity entity)
        {
            _dbContext.Secundary.Add(entity);
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

    }
}
