using coreApi.Models.Interfaces;
using coreApi.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace coreApi.Repository
{
    public class ComposedKeyCrudRepository<TDbContext, TEntity> : IComposedKeyCrudRepository<TEntity> 
        where TDbContext : DbContext
        where TEntity : class, IEntityComposedKey<TEntity>
    {
        protected TDbContext dbContext;

        public ComposedKeyCrudRepository(TDbContext context)
        {
            dbContext = context;
        }

        public async Task CreateAsync(TEntity entity) 
        {
            this.dbContext.Set<TEntity>().Add(entity);

            _ = await this.dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(TEntity entity)
        {
            this.dbContext.Set<TEntity>().Remove(entity);

            _ = await this.dbContext.SaveChangesAsync();
        }

       public async Task<List<TEntity>> SelectAllAsync() 
        {
            return await this.dbContext.Set<TEntity>().ToListAsync();
        }


        public async Task<TEntity> SelectByIdAsync(params object[] keyValues)
        {
            return await this.dbContext.Set<TEntity>().FindAsync(keyValues);
        }

        public async Task UpdateAsync(TEntity entity) 
        {
            this.dbContext.Set<TEntity>().Update(entity);

            _ = await this.dbContext.SaveChangesAsync();
        }
    }
}   