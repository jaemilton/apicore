using coreApi.Models.Interfaces;
using coreApi.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace coreApi.Repository
{
    public class SimpleKeyCrudRepository<TDbContext, TEntity, TKey> : ISimpleKeyCrudRepository<TEntity, TKey> 
        where TDbContext : DbContext
        where TEntity : class, IEntityKey<TKey>
    {
        protected TDbContext dbContext;

        public SimpleKeyCrudRepository(TDbContext context)
        {
            dbContext = context;
        }

        public async Task CreateAsync(TEntity entity) 
        {
            this.dbContext.Set<TEntity>().Add(entity);

            _ = await this.dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(TEntity entity)
        {
            this.dbContext.Set<TEntity>().Remove(entity);

            _ = await this.dbContext.SaveChangesAsync();
        }

       public async Task<List<TEntity>> SelectAllAsync() 
        {
            return await this.dbContext.Set<TEntity>().ToListAsync();
        }


        public async Task<TEntity> SelectByIdAsync(TKey id)
        {
            return await this.dbContext.Set<TEntity>().FindAsync(id);
        }

        public async Task UpdateAsync(TEntity entity) 
        {
            this.dbContext.Set<TEntity>().Update(entity);

            _ = await this.dbContext.SaveChangesAsync();
        }
    }
}   