﻿using coreApi.Common.BackgroundTasks;
using coreApi.Common.Interfaces;
using coreApi.Models.Enitities;
using coreApi.Repository.Interfaces;
using coreApi.Util.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace coreApi.Services
{
    public class LongTaskAsyncRunnerService : IDoWorkAsync
    {
        private readonly ICustomLogger _customLogger;
        private readonly ILongRunnerTaskRepository _longRunnerTaskRepository;
        public LongTaskAsyncRunnerService
        (
            ICustomLogger customLogger,
            ILongRunnerTaskRepository longRunnerTaskRepository
        )
        {
            _customLogger = customLogger;
            _longRunnerTaskRepository = longRunnerTaskRepository;
        }

        public async Task DoWorkAsync(CancellationToken stoppingToken, params object[] parameters)
        {
            var taskName = parameters[0];
            // Simulate three 5-second tasks to complete
            // for each enqueued work item

            var guid = Guid.NewGuid().ToString();

            _customLogger.WriteLog($"Queued Background Task {guid} is starting.");
            bool taskFinished = false;

            while (!stoppingToken.IsCancellationRequested && !taskFinished)
            {
                try
                {

                    var task =
                            new TaskRunner()
                            {
                                DataHoreaInicio = DateTime.Now
                            };

                    _customLogger.WriteLog($"Starting Task {taskName}");
                    await Task.Delay(TimeSpan.FromSeconds(15), stoppingToken);
                    task.DataHoraFim = DateTime.Now;
                    _longRunnerTaskRepository.SaveSmsMessage(task);
                    _longRunnerTaskRepository.Commit();
                    _customLogger.WriteLog($"Finished Task {taskName}");
                    taskFinished = true;

                }
                catch (OperationCanceledException)
                {
                    // Prevent throwing if the Delay is cancelled
                }

                _customLogger.WriteLog($"Queued Background Task {guid} is running.");
            }

            if (taskFinished)
            {
                _customLogger.WriteLog($"Queued Background Task {guid} is complete.");
            }
            else
            {
                _customLogger.WriteLog($"Queued Background Task {guid} was cancelled.");
            }
        }
    }
}
