﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace coreApi.Services
{
    public static class ServicesHelper
    {
        public static void AddServicesDependecyInjection(this IServiceCollection services)
        {
            services.AddTransient<LongTaskAsyncRunnerService>();
        }
    }
}
