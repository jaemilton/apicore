using coreApi.Common;
using coreApi.Common.BackgroundTasks;
using coreApi.Common.Interfaces;
using coreApi.Models;
using coreApi.Models.Entities;
using coreApi.Repository;
using coreApi.Repository.Context;
using coreApi.Repository.Interfaces;
using coreApi.Services;
using coreApi.Util;
using coreApi.Util.Enums;
using coreApi.Util.Middlewares;
using coreApi.Workflow;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace coreApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //https://docs.microsoft.com/en-us/ef/core/dbcontext-configuration/
            //The AddDbContext extension method registers DbContext types with a scoped lifetime by default.
            //services.AddDbContext<AppDBContext>();
            services.AddDbContext<AppDBContext>(contextLifetime: ServiceLifetime.Transient, optionsLifetime: ServiceLifetime.Transient) ;
            services.AddDbContext<RelatedDataDBContext>();
            services.AddDbContext<WeardDbContext>();


            //https://docs.microsoft.com/en-us/ef/core/performance/advanced-performance-topics?tabs=with-constant#dbcontext-pooling
            //AddDbContextPool enables a pool of reusable context instances.
            //To use context pooling, use the AddDbContextPool method instead of AddDbContext during service registration:
            services.AddDbContextPool<ConcurrencyDBContext>(
                (DbContextOptionsBuilder options) => 
                {
                    options.UseSqlServer(Configuration.GetConnectionString("default"));
                }
            );

            services.AddDbContextPool<LongRunnerTaskDbContext>(
               (DbContextOptionsBuilder options) =>
               {
                   options.UseSqlServer(Configuration.GetConnectionString("default"));
               }
           );

            //services.AddHealthChecks("/actuator/health");
            services.AddHealthChecks();
            services.AddControllers();

            services.AddHttpClient();
            services.AddLogging();

            services.AddScoped<ISimpleKeyCrudRepository<MemberEntity, int>, SimpleKeyCrudRepository<AppDBContext, MemberEntity, int>>();
            services.AddScoped<IComposedKeyCrudRepository<ConcurrencyEntity>, ComposedKeyCrudRepository<ConcurrencyDBContext, ConcurrencyEntity>>();

            
            services.AddScoped<IRelatedDataRepository, RelatedDataRepository>();
            services.AddScoped<ILongRunnerTaskRepository, LongRunnerTaskRepository>();

            services.AddScoped<IWeardEntityRepository, WeardEntityRepository>();
            services.AddTransient<ScopedService>();


            //services.StartServiceCollectionHelper();
            services.AddCommomServiceDependecyInjection();
            services.AddServicesDependecyInjection();
            
            
            services.AddTransient<LongTaskAsyncRunnerService>();
            services.AddHostedService<QueuedTaskRunner>();
            services.AddSingleton<IBackgroundTaskQueue>(ctx => {
                if (!int.TryParse(Configuration["QueueCapacity"], out var queueCapacity))
                    queueCapacity = 100;
                return new BackgroundTaskQueue(queueCapacity);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseMiddleware<LogMiddleware>(LoggerType.Console);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/actuator/health");
                endpoints.MapControllers();
            });

            
        }
    }
}
