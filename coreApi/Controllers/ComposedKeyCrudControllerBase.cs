﻿using coreApi.Models.Interfaces;
using coreApi.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace coreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class ComposedKeyCrudControllerBase<TEntity> : ControllerBase 
        where TEntity : class, IEntityComposedKey<TEntity>
    {
        protected readonly IComposedKeyCrudRepository<TEntity> _repository;
        public ComposedKeyCrudControllerBase(IComposedKeyCrudRepository<TEntity> repository)
        {
            _repository = repository;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<TEntity>>> GetAsync()
        {
            return await _repository.SelectAllAsync();
        }

        public async Task<ActionResult<TEntity>> GetByIdAsync(params object[] keyValues)
        {
            var model = await _repository.SelectByIdAsync(keyValues);

            if (model == null)
            {
                return NotFound();
            }

            return model;
        }


        
        public async Task<IActionResult> UpdateAsync([FromBody] TEntity model, [FromRoute] params object[] keyValues)
        {
            if (!keyValues.Equals(model.KeyValues))
            {
                return BadRequest();
            }

            await _repository.UpdateAsync(model);

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<TEntity>> InsertAsync(TEntity model)
        {
            await _repository.CreateAsync(model);
            return CreatedAtAction("GetMember", new { id = model.KeyValuesToString }, model);
        }

        public async Task<ActionResult<TEntity>> DeleteAsync(params object[] keyValues)
        {
            var model = await _repository.SelectByIdAsync(keyValues);

            if (model == null)
            {
                return NotFound();
            }

            await _repository.DeleteAsync(model);

            return model;
        }

        protected object[] GetKeyValues(params object[] keyValues)
        {
            return keyValues;
        }
    }
}
