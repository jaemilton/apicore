﻿using coreApi.Models.Enitities.RelatedData;
using coreApi.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace coreApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RelatedDataController : Controller
    {
        private readonly IRelatedDataRepository _repository;
        public RelatedDataController(IRelatedDataRepository repository)
        {
            _repository = repository;
        }

        [HttpPost]
        public ActionResult<MessageSmsEntity> SmsMessages(MessageSmsEntity entity)
        {
            _repository.SaveSmsMessage(entity);
            _repository.Commit();
            return CreatedAtAction($"SmsMessages", new { id = entity.Id }, entity);
        }

        [HttpPost]
        public ActionResult<MessageEmailEntity> EmailMessages(MessageEmailEntity entity)
        {
            _repository.SaveEmailMessage(entity);
            _repository.Commit();
            return CreatedAtAction($"EmailMessages", new { id = entity.Id }, entity);
        }

        [HttpPost]
        public ActionResult<MessageKeySmsEntity> KeySmsMessages(MessageKeySmsEntity entity)
        {
            _repository.SaveKeySmsMessage(entity);
            _repository.Commit();
            return CreatedAtAction($"KeySmsMessages", new { id = $"{entity.CodeP1}_{entity.CodeP2}_{entity.KeyDate}_{entity.Key}" }, entity);
        }
    }
}
