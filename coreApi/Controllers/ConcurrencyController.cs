﻿using coreApi.Models;
using coreApi.Models.Entities;
using coreApi.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace coreApi.Controllers
{
    
    public class ConcurrencyController : ComposedKeyCrudControllerBase<ConcurrencyEntity>
    {

        public ConcurrencyController(IComposedKeyCrudRepository<ConcurrencyEntity> repository) : base(repository)
        {
        }

        private const string KEY_TEMPLATE = "{codigoTelefonePessoa}/{codigoCliente}/{dataMensagem}/{codigoOferta}";

        [HttpGet(KEY_TEMPLATE)]
        public async Task<ActionResult<ConcurrencyEntity>> GetByIdAsync
        (
            string codigoTelefonePessoa, 
            string codigoCliente, 
            DateTime dataMensagem, 
            string codigoOferta
        )
        {
            return await base.GetByIdAsync(GetKeyValues(codigoTelefonePessoa, codigoCliente, dataMensagem, codigoOferta));
        }


        [HttpPut(KEY_TEMPLATE)]
        public async Task<IActionResult> UpdateAsync([FromBody] ConcurrencyEntity model, 
            string codigoTelefonePessoa,
            string codigoCliente,
            DateTime dataMensagem,
            string codigoOferta)
        {
            return await base.UpdateAsync(model, GetKeyValues(codigoTelefonePessoa, codigoCliente, dataMensagem, codigoOferta));
        }

       

        [HttpDelete(KEY_TEMPLATE)]
        public async Task<ActionResult<ConcurrencyEntity>> DeleteAsync
        (
            string codigoTelefonePessoa,
            string codigoCliente,
            DateTime dataMensagem,
            string codigoOferta
        )
        {
            return await base.DeleteAsync(GetKeyValues(codigoTelefonePessoa, codigoCliente, dataMensagem, codigoOferta));
        }

    }
}
