﻿using coreApi.Common;
using coreApi.Common.BackgroundTasks;
using coreApi.Common.Interfaces;
using coreApi.Services;
using coreApi.Util.RequestsInfo;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace coreApi.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class LongRunnerTaskController : Controller
    {
        private readonly IHostApplicationLifetime _applicationLifetime;
        private IBackgroundTaskQueue Queue { get; }

        public LongRunnerTaskController(IBackgroundTaskQueue queue, IHostApplicationLifetime applicationLifetime, ScopedService scopedService)
        {
            Queue = queue;
            _applicationLifetime = applicationLifetime;
        }


        [HttpPost()]
        public async Task<IActionResult> RunLongRunningTaskAsync([FromBody] string id)
        {
            // Enqueue a background work item
            await 
                Queue.QueueBackgroundWorkItemAsync
                (
                    new BackgroundTask<LongTaskAsyncRunnerService>()
                    {
                        Parameters = new object[] { id },
                        StoppingToken = _applicationLifetime.ApplicationStopping,
                        OriginalRequestInfo = await RequestInfo.CreateInstanceAsync(HttpContext)
                    }
                );
        
            return Accepted($"GET /api/LongRunnerTask/{id}");
        }

        [HttpGet("{id}")]
        public ActionResult<string> GetRunLongRunningTask([FromRoute] string id)
        {
            return Ok(new { id = id } );
        }
    }
}
