﻿using coreApi.Models.Interfaces;
using coreApi.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace coreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SimpleKeyCrudControllerBase<TEntity, TKey> : ControllerBase 
        where TEntity : class, IEntityKey<TKey>
    {
        protected readonly ISimpleKeyCrudRepository<TEntity, TKey> _repository;
        public SimpleKeyCrudControllerBase(ISimpleKeyCrudRepository<TEntity, TKey> repository)
        {
            _repository = repository;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<TEntity>>> GetAsync()
        {
                return await _repository.SelectAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TEntity>> GetByIdAsync(TKey id)
        {
            var model = await _repository.SelectByIdAsync(id);

            if (model == null)
            {
                return NotFound();
            }

            return model;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(TKey id, TEntity model)
        {
            if (!id.Equals(model.Id))
            {
                return BadRequest();
            }

            await _repository.UpdateAsync(model);

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<TEntity>> InsertAsync(TEntity model)
        {
            await _repository.CreateAsync(model);
            return CreatedAtAction($"{this.ControllerContext.ActionDescriptor.ActionName}", new { id = model.Id }, model);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<TEntity>> DeleteAsync(TKey id)
        {
            var model = await _repository.SelectByIdAsync(id);

            if (model == null)
            {
                return NotFound();
            }

            await _repository.DeleteAsync(model);

            return model;
        }
    }
}
