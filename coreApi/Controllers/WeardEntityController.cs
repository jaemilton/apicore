﻿using coreApi.Models.Enitities.Weard;
using coreApi.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace coreApi.Controllers
{

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class WeardEntityController : Controller
    {

        private readonly IWeardEntityRepository _repository;
        public WeardEntityController(IWeardEntityRepository repository)
        {
            _repository = repository;
        }

        public ActionResult<SecundaryWeardEntity> CreateSecundary([FromBody] SecundaryWeardEntity entity)
        {
            _repository.AddSecundary(entity);
            _repository.Save();
            return CreatedAtAction($"CreateSecundary", new { id = entity.Id }, entity);
        }
    }
}
