using System.Collections.Generic;
using System.Threading.Tasks;
using coreApi.Models;
using coreApi.Models.Entities;
using coreApi.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
namespace coreApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MembersController : SimpleKeyCrudControllerBase<MemberEntity, int>
    {
        public MembersController(ISimpleKeyCrudRepository<MemberEntity, int> repository): base(repository)
        {
        }

    }
}