﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace coreApi.Util.Interfaces
{
    public interface ICustomLogger
    {


        List<string> Messages { get; }
        List<Task> Tasks { get; }

        void AddTask(Task task);
        void WriteLog(string message);
        void WriteLog(Exception ex, string message);
    }
}