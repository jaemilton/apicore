﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace coreApi.Util
{
    public class CustomLog
    {
        [JsonPropertyName("assembly")]
        public string Assembly { get; set; }
        
        [JsonPropertyName("start_timestamp")]
        public DateTime StartDateTime { get; set; }

        [JsonPropertyName("messages")]
        public string Messages { get; set; }

        [JsonPropertyName("finish_timestamp")]
        public DateTime FinishedDateTime { get; set; }

        [JsonPropertyName("duration")]
        public string Duration 
        {
            get
            {
                return (FinishedDateTime - StartDateTime).ToString();
            }
        }
    }
}
