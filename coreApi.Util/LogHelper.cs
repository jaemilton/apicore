﻿using coreApi.Util.Enums;
using coreApi.Util.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace coreApi.Util
{
    public static class LogHelper
    {
        public static void AddCommomServiceDependecyInjection(this IServiceCollection services)
        {
            services.AddScoped<ICustomLogger, CustomLogger>();
        }

        public static async Task WriteLogsAsync<T>(LoggerType logType, ILogger logger, T customLog, ICustomLogger customLogger) where T: CustomLog
        {
            var hasTaskNotCompleted = true;
            while(hasTaskNotCompleted)
            {
                hasTaskNotCompleted = customLogger.Tasks.Any(x => !x.IsCompleted);
                await Task.WhenAll(customLogger.Tasks);
            }

            await Task.Run(() => WriteLogs(logType, logger, customLog, customLogger));
        }

        public static void WriteLogs<T>(LoggerType logType, ILogger logger, T customLog, ICustomLogger customLogger) where T : CustomLog
        {
            if (customLog != null && customLogger != null && customLogger.Messages.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var message in customLogger.Messages)
                {
                    sb.AppendLine(message);
                }
                customLog.Messages = sb.ToString();
            }
            

            switch (logType)
            {
                case LoggerType.Console:
                    Console.WriteLine(JsonSerializer.Serialize(customLog));
                    break;
                case LoggerType.Logger:
                    logger.LogInformation(JsonSerializer.Serialize(customLog));
                    break;
                case LoggerType.None:
                default:
                    break;
            }
           
        }

    }
}
