﻿using coreApi.Util.RequestsInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace coreApi.Util
{
    public class BackgroundTaskLog: CustomLog
    {
        public RequestInfo OriginRequestInfo { get; set; }
    }
}
