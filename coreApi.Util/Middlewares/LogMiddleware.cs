﻿using coreApi.Util.Enums;
using coreApi.Util.Interfaces;
using coreApi.Util.RequestsInfo;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.IO;

using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace coreApi.Util.Middlewares
{
    public class LogMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<LogMiddleware> _logger;
        private readonly LoggerType _logType;
        private RequestLog log = new RequestLog();

        public LogMiddleware(RequestDelegate next, ILogger<LogMiddleware> logger, LoggerType logType)
        {
            _next = next;
            _logger = logger;
            _logType = logType;
        }

        public async Task InvokeAsync(HttpContext httpContext, ICustomLogger logger)
        {
            if (httpContext.Request.Path == "/actuator/health" || httpContext.Request.Path == "/favicon.ico")
            {
                await _next.Invoke(httpContext);
            }
            else
            {
                //var originalBodyStream = httpContext.Response.Body;

                log.StartDateTime = DateTime.Now;
                log.Assembly = Assembly.GetExecutingAssembly().FullName;
                log.RequestInfo = await RequestInfo.CreateInstanceAsync(httpContext);

                var responseBodyStream = MemoryStreamHelper.StreamManager.GetStream();
                httpContext.Response.Body = responseBodyStream;

                // Call the next middleware delegate in the pipeline 
                await _next.Invoke(httpContext);

                log.ResponseInfo = await ResponseInfo.CreateInstanceAsync(httpContext);

                log.FinishedDateTime = DateTime.Now;
                //Save logs here
                await LogHelper.WriteLogsAsync(_logType, _logger, log, logger);
                //await responseBodyStream.CopyToAsync(originalBodyStream);
            }

        }
       

    }
}
