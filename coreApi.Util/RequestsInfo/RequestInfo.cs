﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace coreApi.Util.RequestsInfo
{
    public class RequestInfo : RequestBase
    {
        private RequestInfo(HttpRequest request, string body)
        {
            Protocol = request.Protocol;
            Host = request.Host;
            Method = request.Method;
            PathBase = request.PathBase;
            Path = request.Path;
            Headers = request.Headers.ToDictionary(k => k.Key, v => v.Value);
            Query = request.Query.ToDictionary(k => k.Key, v => v.Value);
            QueryString = request.QueryString;
            ContentType = request.ContentType;
            Cookies = request.Cookies.ToDictionary(k => k.Key, v => v.Value);
            Body = body;
        }

        public string Protocol { get; private set; }
        public HostString Host { get; private set; }
        public string Method { get; private set; }
        public PathString PathBase { get; private set; }
        public PathString Path { get; private set; }
        public Dictionary<string, StringValues> Query { get; private set; }
        public QueryString QueryString { get; private set; }
        public Dictionary<string, string> Cookies { get; private set; }


        public static async Task<RequestInfo> CreateInstanceAsync(HttpContext context)
        {
            context.Request.EnableBuffering();

            using (var requestStream = MemoryStreamHelper.StreamManager.GetStream())
            {
                await context.Request.Body.CopyToAsync(requestStream);
                context.Request.Body.Position = 0;
                using (StreamReader sr = new StreamReader(requestStream))
                {
                    requestStream.Seek(0, SeekOrigin.Begin);
                    var requestBody = await sr.ReadToEndAsync();
                    return new RequestInfo(context.Request, requestBody);
                }
            }
        }
    }

    

}
