﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace coreApi.Util.RequestsInfo
{
    public class ResponseInfo : RequestBase
    {
        private ResponseInfo(HttpResponse response, string body) 
        {
            StatusCode = response.StatusCode;
            ContentType = response.ContentType;
            Headers = response.Headers.ToDictionary(k => k.Key, v => v.Value);
            Cookies = response.Cookies;
            Body = body;
        }

        public int StatusCode { get; private set; }
        public IResponseCookies Cookies { get; private set; }

        public static async Task<ResponseInfo> CreateInstanceAsync(HttpContext context)
        {
            await using(var responseStream = MemoryStreamHelper.StreamManager.GetStream())
            {
                await context.Response.Body.CopyToAsync(responseStream);
                responseStream.Seek(0, SeekOrigin.Begin);
                using (StreamReader sr = new StreamReader(responseStream))
                {
                    var responseBody = await sr.ReadToEndAsync();
                    var retorno =  new ResponseInfo(context.Response, responseBody);
                    context.Response.Body.Seek(0, SeekOrigin.Begin);
                    return retorno;
                }
            }
            
        }
    }
}
