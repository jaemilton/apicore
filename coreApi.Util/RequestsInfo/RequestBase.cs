﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Microsoft.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace coreApi.Util.RequestsInfo
{
    public class RequestBase
    {
        public Dictionary<string, StringValues> Headers { get; protected set; }
        public string ContentType { get; protected set; }
        public string Body { get; protected set; }
        
    }
}
