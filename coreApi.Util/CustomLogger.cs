﻿using coreApi.Util.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace coreApi.Util
{
    public class CustomLogger : ICustomLogger
    {

        public CustomLogger()
        {
            Messages = new List<string>();
            Tasks = new List<Task>();
        }
        public List<string> Messages { get; private set; }
        public List<Task> Tasks { get; private set; }

        public void WriteLog(string message)
        {
            Messages.Add(message);
        }

        public void WriteLog(Exception ex, string message)
        {
            Messages.Add(message);
        }
        public void AddTask(Task task)
        {
            Tasks.Add(task);
        }


    }
}
