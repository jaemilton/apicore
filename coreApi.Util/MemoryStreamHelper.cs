﻿using Microsoft.IO;
using System;
using System.Collections.Generic;
using System.Text;

namespace coreApi.Util
{

    public static class MemoryStreamHelper
    {
        private static int blockSize = 1024;
        private static int largeBufferMultiple = 1024 * 1024;
        private static int maximumBufferSize = 16 * largeBufferMultiple;
        private static int maximumFreeLargePoolBytes = maximumBufferSize * 4;
        private static int maximumFreeSmallPoolBytes = 250 * blockSize;

        public static readonly RecyclableMemoryStreamManager _recyclableMemoryStreamManager;

        static MemoryStreamHelper()
        {
            _recyclableMemoryStreamManager =
                new RecyclableMemoryStreamManager(blockSize, largeBufferMultiple, maximumBufferSize);
            _recyclableMemoryStreamManager.AggressiveBufferReturn = true;
            _recyclableMemoryStreamManager.GenerateCallStacks = true;
            _recyclableMemoryStreamManager.MaximumFreeLargePoolBytes = maximumFreeLargePoolBytes;
            _recyclableMemoryStreamManager.MaximumFreeSmallPoolBytes = maximumFreeSmallPoolBytes;
        }

        public static RecyclableMemoryStreamManager StreamManager { get => _recyclableMemoryStreamManager; }

    }
}
