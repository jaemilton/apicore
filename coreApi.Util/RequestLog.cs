﻿using coreApi.Util.RequestsInfo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace coreApi.Util
{
    public class RequestLog : CustomLog
    {
        [JsonPropertyName("request")]
        public RequestInfo RequestInfo { get; set; }

        [JsonPropertyName("response")]
        public ResponseInfo ResponseInfo { get; set; }

    }
}
