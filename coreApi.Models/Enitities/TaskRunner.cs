﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace coreApi.Models.Enitities
{
    [Table("TB_TASK")]
    public class TaskRunner
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("COD_TASK", TypeName = "int")]
        public int Id { get; set; }

        [Column("DAT_HORA_INCI", TypeName = "datetime")]
        [Required]
        public DateTime DataHoreaInicio { get; set; }

        [Column("DAT_HORA_FIM", TypeName = "datetime")]
        [Required]
        public DateTime DataHoraFim { get; set; }
    }
}
