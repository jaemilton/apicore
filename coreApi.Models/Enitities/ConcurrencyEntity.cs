﻿using coreApi.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace coreApi.Models.Entities
{
    

    [Table("TB_CONC")]
    public class ConcurrencyEntity : EntityComposedKey<ConcurrencyEntity>
    {
        static ConcurrencyEntity()
        {
            ConcurrencyEntity.KeyExpression = c => new { c.CodigoTelefonePessoa, c.CodigoCliente, c.DataMensagem, c.CodigoOferta };
        }

        [Column("COD_TELE_PESS", TypeName = "nvarchar(50)")]
        public string CodigoTelefonePessoa { get; set; }

        [Column("COD_CLI", TypeName = "nvarchar(14)")]
        public string CodigoCliente { get; set; }

        [Column("DAT_MENS", TypeName = "date")]
        public DateTime DataMensagem { get; set; }

        [Column("COD_OFER", TypeName = "nvarchar(10)")]
        public string CodigoOferta { get; set; }

        [Column("COD_MES", TypeName = "int")]
        public int? CodigoMensagem { get; set; }
    }
}
