﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace coreApi.Models.Enitities.RelatedData
{
    [Table("TB_MENS_SMS")]
    public class MessageSmsEntity
    {
        [Key]
        [Column("COD_MENS")]
        public int Id { get; set; }

        [Column("ID_CELU")]
        [Required]
        [MaxLength(36)]
        public string CellphoneId { get; set; }

        public MessageEntity Message { get; set; }

    }
}
