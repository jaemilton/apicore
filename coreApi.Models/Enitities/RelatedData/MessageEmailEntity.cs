﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace coreApi.Models.Enitities.RelatedData
{
    [Table("TB_MENS_EMAIL")]
    public class MessageEmailEntity
    {
        [Key]
        [Column("COD_MENS")]
        public int Id { get; set; }

        [Column("ID_EMAIL")]
        [Required]
        [MaxLength(36)]
        public string EmailId { get; set; }

        public MessageEntity Message { get; set; }
    }
}
