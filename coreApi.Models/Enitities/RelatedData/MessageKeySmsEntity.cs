﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace coreApi.Models.Enitities.RelatedData
{
    [Table("TB_CHAVE_MENS_SMS")]
    public class MessageKeySmsEntity
    {
        [Column("COD_P1")]
        [MaxLength(3)]
        [Required]
        public string CodeP1 { get; set; }

        [Column("COD_P2")]
        [MaxLength(4)]
        [Required]
        public string CodeP2 { get; set; }

        [Column("DAT_CHAVE")]
        [Required]
        public DateTime KeyDate { get; set; }

        [Column("TXT_CHAVE")]
        [MaxLength(10)]
        //[Required]
        public string Key { get; set; }

        [Column("COD_MENS")]
        public int IdSmsMessage { get; set; }

        public MessageSmsEntity SmsMessage { get; set; }

    }
}
