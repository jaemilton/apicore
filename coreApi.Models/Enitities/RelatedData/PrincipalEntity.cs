﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace coreApi.Models.Enitities.RelatedData
{
    [Table("TB_PRIN")]
    public class PrincipalEntity
    {
        [Column("COD_P1")]
        [MaxLength(2)]
        [Required]
        public string CodeP1 { get; set; }

        [Column("COD_P2")]
        [MaxLength(4)]
        [Required]
        public string CodeP2 { get; set; }

        [Column("COD_SITU")]
        [Required]
        public byte SituationId { get; set; }

        public SituationTypeEntity Situation { get; set; }

    }
}
