﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace coreApi.Models.Enitities.RelatedData
{
    [Table("TB_MENS")]
    public class MessageEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("COD_MENS")]
        public int Id { get; set; }

        [Column("COD_P1")]
        [MaxLength(3)]
        [Required]
        public string CodeP1 { get; set; }

        [Column("COD_P2")]
        [MaxLength(4)]
        [Required]
        public string CodeP2 { get; set; }

        [Column("DAT_MENS")]
        [Required]
        public DateTime InsertDate { get; set; }

        [Column("COD_MENS_EXT")]
        [MaxLength(36)]
        //[Required]
        public string ExternalMessageCode { get; set; }

        public PrincipalEntity Principal { get; set; }
    }
}
