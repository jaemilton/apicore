﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace coreApi.Models.Enitities.RelatedData
{
    [Table("TB_SITU")]
    public class SituationTypeEntity
    {
        [Key]
        [Column("COD_SITU")]
        public byte Id { get; set; }

        [Column("NOM_SITU")]
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
    }
}
