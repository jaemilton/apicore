﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace coreApi.Models.Enitities.Weard
{
    [Table("TB_WEAD_SECD")]
    public class SecundaryWeardEntity
    {
        [Column("COD_WEAD_SECD")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column("DAT_INCL")]
        public DateTime CreatedDate { get; set; }

        [Column("COD_WEAD_PRIN")]
        public int? PrincipalId { get; set; }

    }
}
