﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace coreApi.Models.Enitities.Weard
{
    [Table("TB_WEAD_PRIN")]
    public class PrincipalWeardEntity
    {
        
        [Key]
        [Column("COD_WEAD_PRIN")]
        public int Id { get; set; }

        [Column("DAT_INCL")]
        [Required]
        public DateTime CreatedDate { get; set; }

    }
}
