using coreApi.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace coreApi.Models.Entities
{
    [Table("TB_MBRO")]
    public class MemberEntity : IEntityKey<int>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("COD_MBRO", TypeName = "int")]
        public int Id { get; set; }

        [Column("PMRO_NOME_MBRO", TypeName = "nvarchar(50)")]
        public string FirstName { get; set; }

        [Column("ULTM_NOME_MBRO", TypeName = "nvarchar(50)")]
        public string LastName { get; set; }

        [Column("NOME_MEIO", TypeName = "nvarchar(50)")]
        public string MiddleName { get; set; }
    }
}