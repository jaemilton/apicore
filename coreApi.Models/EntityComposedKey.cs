﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.Json.Serialization;

namespace coreApi.Models.Interfaces
{
    public abstract class EntityComposedKey<TEntity> : IEntityComposedKey<TEntity>
    {
        public static Expression<Func<TEntity, object>> KeyExpression { get; protected set; }

        [JsonIgnore]
        public string KeyValuesToString
        {
            get
            {
                return string.Join("-", KeyValues);
            }
        }

        [JsonIgnore]
        public object[] KeyValues
        {
            get
            {
                if (KeyExpression == null || KeyExpression.Body.Type.GetProperties().Count() < 2)
                {
                    throw new ArgumentNullException("The property names must be specifyed and must have more than one property name");
                }

                return this.GetType()
                    .GetProperties()
                    .Where(l => KeyExpression.Body.Type.GetProperties().Any(x => x.Name.Equals(l.Name)))
                    .Select(l => l.GetValue(this, null)).ToArray();
            }
            
        }
    }
}
