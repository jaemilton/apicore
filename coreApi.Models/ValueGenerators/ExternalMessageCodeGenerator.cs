﻿using coreApi.Models.Enitities.RelatedData;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Threading;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace coreApi.Models.ValueGenerators
{
    public class ExternalMessageCodeGenerator : ValueGenerator<string>
    {

        private const string EXTERNAL_MESSAGE_API_URL_SESSION = "ExternalMessageApiUrl";
        public override bool GeneratesTemporaryValues => false;
        private static JoinableTaskFactory joinableTaskFactory = new JoinableTaskFactory(new JoinableTaskContext());


        public  override string Next([NotNullAttribute] EntityEntry entry)
        {

            // The outer delegate is synchronous, but kicks off async work via a method that accepts an async delegate.
            return joinableTaskFactory.Run<string>(
                async delegate {

                    if (entry is null)
                    {
                        throw new ArgumentNullException(nameof(entry));
                    }
                    var configuration = entry.Context.GetService<IConfiguration>();
                    var url = configuration[EXTERNAL_MESSAGE_API_URL_SESSION];
                    var request =
                        new HttpRequestMessage(HttpMethod.Get, url);

                    var clientFactory = entry.Context.GetService<IHttpClientFactory>();

                    var client = clientFactory.CreateClient();
                    var response = await client.SendAsync(request);

                    if (!response.IsSuccessStatusCode)
                    {
                        throw new HttpRequestException($"Error requesting ExternalMessageCode from {url}, StatusCode = {response.StatusCode}, ReasonPhrase: {response.ReasonPhrase}");
                    }

                    var stringResult = await response.Content.ReadAsStringAsync();
                    var result = JsonSerializer.Deserialize<List<string>>(stringResult);
                    if (result != null && result.Count > 0)
                        return result.First();
                    else
                        throw new HttpRequestException($"There is no result from GET {url}");
                }
            );

        }

    }
}
