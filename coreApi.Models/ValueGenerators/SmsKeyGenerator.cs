﻿using coreApi.Models.Enitities.RelatedData;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace coreApi.Models.ValueGenerators
{
    public class SmsKeyGenerator : ValueGenerator<string>
    {
        string SMS_KEY_FIXED_PART_SESSION = "SmsKeyFixedPart";

        public override bool GeneratesTemporaryValues => false;

        public  override string Next(EntityEntry entry)
        {
            if (entry is null)
            {
                throw new ArgumentNullException(nameof(entry));
            }

            var configuration = entry.Context.GetService<IConfiguration>();

            var smsKeyFixedPart = configuration[SMS_KEY_FIXED_PART_SESSION];
            var messageKeySms = (MessageKeySmsEntity)entry.Entity;
            var x = entry.Context.Set<MessageKeySmsEntity>().AsNoTracking()
                .Where
                (x =>
                    x.CodeP1 == messageKeySms.CodeP1 &&
                    x.CodeP2 == messageKeySms.CodeP2
                ).ToList();

            var maxSmsKey = 
                 entry.Context.Set<MessageKeySmsEntity>().AsNoTracking()
                .Where
                (x =>
                    x.CodeP1 == messageKeySms.CodeP1 &&
                    x.CodeP2 == messageKeySms.CodeP2 &&
                    x.KeyDate == DateTime.Today
                )
                .Max(x => x.Key);

            var newMaxSmsKey = string.IsNullOrEmpty(maxSmsKey) ?
                "1" :
                (byte.Parse(maxSmsKey.Substring(smsKeyFixedPart.Length)) + 1).ToString();
            return 
                $"{smsKeyFixedPart}{newMaxSmsKey.PadLeft(2, '0')}";

        }
    }
}
