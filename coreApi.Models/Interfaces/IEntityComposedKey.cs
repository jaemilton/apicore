﻿using System;
using System.Linq.Expressions;

namespace coreApi.Models.Interfaces
{
    public interface IEntityComposedKey<TEntity>
    {
        public static Expression<Func<TEntity, object>> KeyExpression { get; }

        public object[] KeyValues { get; }

        public string KeyValuesToString { get; }
    }
}