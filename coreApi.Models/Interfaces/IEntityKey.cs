﻿
namespace coreApi.Models.Interfaces
{
    public interface IEntityKey<TKey> 
    {
        public TKey Id { get; }
    }
}
